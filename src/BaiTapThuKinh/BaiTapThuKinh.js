import React, { Component } from "react";
import dataGlasses from "../Data/dataGlasses.json";

export default class BaiTapThuKinh extends Component {
  state = {
    glassesCurrent: {
      'id':  'G1',
      'virtualImg': './img/v1.png',
      'brand': 'Armani Exchange',
      'name': 'Bamboo wood',
      'price': 150,
      'description':
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis ea voluptates officiis',
    },
  };

  renderGlassesList = () => {
    return dataGlasses.map((glassesItem, index) => {
      return (
        <img
          onClick={() => {this.changeGlasses(glassesItem)}}
          className="ml-2 p-2 border border-width-1"
          style={{ width: "110px", cursor: "pointer" }}
          key={index}
          src={glassesItem.virtualImg}
        />
      );
    });
  };

  changeGlasses = (newGlasses) => {
    this.setState( {
      glassesCurrent: newGlasses,
    })
  }

  render() {

const keyFrame = `@keyframes animChangeGlass${Date.now()}{
  from{
  width: 0;
  transform:rotate(45deg);
  opacity:0
  }
  to{
  width: 150px;
  transform:rotate(0deg);
  opacity:0.7


  }
  
  }`



    
    const styleGlasses = {
      width: "150px",
      top: "75px",
      right: "70px",
      opacity: "0.7",
      animation: `animChangeGlass${Date.now()} 1s`
    };

    const infoGlasses = {
      width: "250px",
      top: "200px",
      left: "270px",
      paddingLeft: "15px",
      backgroundColor: "rgba(255,127,0,.5)",
      textAlign: "left",
      height: "105px",
    };

    return (
      <div
        style={{
          backgroundImage: "url(./img/bg-img.jpg)",
          backgroundSize: "2000px",
          minHeight: "2000px",
        }}
      >
        <style>{keyFrame}</style>
        <div
          style={{ backgroundColor: "rgba(0,0,0,.65)", minHeight: "2000px" }}
        >
          <h3
            style={{ backgroundColor: "rgba(0,0,0,.3)" }}
            className="text-center text-light p-5"
          >
            TRY GLASSES APP ONLINE
          </h3>
          <div className="container">
            <div className="row mt-5 text-center">
              <div className="col-6">
                <div className="position-relative">
                  <img
                    className="position-absolute"
                    style={{ width: "250px" }}
                    src="./img/model.jpg"
                    alt="model.jpg"
                  />
                  <img
                    style={styleGlasses}
                    className="position-absolute glassesStyle"
                    src={this.state.glassesCurrent.virtualImg}
                    alt=""
                  />
                  <div style={infoGlasses} className="position-relative">
                    <span
                      style={{ color: "#AB82FF", fontSize: "15px" }}
                      className="font-weight-bold"
                    >
                     {this.state.glassesCurrent.id}: {this.state.glassesCurrent.brand} ({this.state.glassesCurrent.name})    
                    </span>
                    <span style={{color:"green",marginLeft:"50px", fontSize:"12px"}}>${this.state.glassesCurrent.price}</span>
                    <br />
                    <span style={{ fontSize: "10px", marginRight:'5px' ,fontWeight: "400" }}>
                      {this.state.glassesCurrent.description}
                    </span>
                  </div>
                </div>
              </div>
              <div className="col-6">
                <img
                  style={{ width: "250px" }}
                  src="./img/model.jpg"
                  alt="model.jpg"
                />
              </div>
            </div>
          </div>
          <div
            className="bg-light container text-center mt-5 d-flex justify-content-center
          p-5"
          >
            {this.renderGlassesList()}
          </div>
        </div>
      </div>
    );
  }
}
//b1: tạo giao diện
